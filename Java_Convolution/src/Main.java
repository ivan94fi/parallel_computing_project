import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {
    private static final int ROUNDS = 16; // number of iterations to evaluate speed-up.
    private static final int NTHREADS = Runtime.getRuntime().availableProcessors();

    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("Error. Please specify the type of convolution and the input image path.");
                return;
            }
            if (args[0].equals("-h")) {
                System.out.println("Java convolution: applies convolution with a gaussian kernel to an input image in jpg format.");
                System.out.println("Usage: Main OPTION INPUTFILEPATH");
                System.out.println("-seq               " + "apply sequential 2D convolution.");
                System.out.println("-par               " + "apply parallel 2D convolution.");
                System.out.println("-seq_sep           " + "apply sequential separable convolution.");
                System.out.println("-par_sep           " + "apply parallel separable convolution.");
                return;
            }

            final RGBImage img = new RGBImage(args[1]);
            final float[][] pixelMatrix = img.getMatrixImage();
            final int imgHeight = img.getHeight();
            final int imgWidth = img.getWidth();
            float[][] outputMatrix = new float[imgHeight][imgWidth * 3];
            float[][] tmpOutputMatrix = new float[imgHeight][imgWidth * 3];
            double timeSpent = 0;
            double timeSpentMilliSec = 0;
            String outputFileName = "";
            System.out.println("Input image: " + img.getName());

            switch (args[0]) {

                //            TEST0
                case "-seq":
                    System.out.println("Sequential Convolution");
                    for (int i = -1; i < ROUNDS; i++) {
                        Util.clearOutputMatrix(outputMatrix);
                        if (i == 0) {
                            timeSpent = 0;
                        }
                        long start = System.nanoTime();
                        Convolution.applyConvolution(pixelMatrix, outputMatrix, imgWidth, imgHeight, 0, imgHeight - 1);
                        long stop = System.nanoTime();
                        timeSpent += stop - start;
                    }
                    timeSpentMilliSec = timeSpent / 1000000;//convert in millisec
                    System.out.println("Execution time: " + timeSpentMilliSec / ROUNDS + " ms");
                    outputFileName = "sequentialConv" + img.getName();
                    break;


                case "-par":
                    //           TEST 1
                    Util.clearOutputMatrix(outputMatrix);
                    System.out.println("Parallel convolution");
                    System.out.printf("Number of threads:%d \n", NTHREADS);
                    for (int i = -1; i < ROUNDS; i++) {
                        Util.clearOutputMatrix(outputMatrix);
                        if (i == -1) {
                            timeSpent = 0;
                        }
                        long start = System.nanoTime();
                        Util.testParallelConvolution(pixelMatrix, outputMatrix, imgWidth, imgHeight, NTHREADS);
                        long stop = System.nanoTime();
                        timeSpent += stop - start;
                    }
                    timeSpentMilliSec = timeSpent / 1000000;
                    System.out.println("Execution time:" + timeSpentMilliSec / ROUNDS + " ms");
                    outputFileName = "parallelConv" + img.getName();
                    break;


                case "-seq_sep":
                    //TEST 2
                    System.out.println("Sequential convolution with separable kernel");
                    for (int i = -1; i < ROUNDS; i++) {
                        Util.clearOutputMatrix(outputMatrix);
                        Util.clearOutputMatrix(tmpOutputMatrix);
                        if (i == 0) {
                            timeSpent = 0;
                        }
                        long start = System.nanoTime();
                        SeparableConvolution.applySeparableConvolution(pixelMatrix, tmpOutputMatrix, outputMatrix, imgWidth, imgHeight, 0, imgHeight - 1, 1);
                        long stop = System.nanoTime();
                        timeSpent += stop - start;
                    }
                    timeSpentMilliSec = timeSpent / 1000000;
                    System.out.println("Execution time: " + timeSpentMilliSec / ROUNDS + " ms");
                    outputFileName = "sequentialSepConv" + img.getName();
                    break;


                // TESTS 3
                case "-par_sep":
                    System.out.println("Separable parallel convolution");
                    System.out.printf("Number of threads:%d \n", NTHREADS);
                    for (int i = -1; i < ROUNDS; i++) {
                        Util.clearOutputMatrix(tmpOutputMatrix);
                        Util.clearOutputMatrix(outputMatrix);
                        if (i == 0) {
                            timeSpent = 0;
                        }
                        long start = System.nanoTime();
                        Util.testParallelSepConvolution(pixelMatrix, tmpOutputMatrix, outputMatrix, imgWidth, imgHeight, NTHREADS);
                        long stop = System.nanoTime();
                        timeSpent += stop - start;
                    }
                    timeSpentMilliSec = timeSpent / 1000000;
                    System.out.println("Execution time: " + timeSpentMilliSec / ROUNDS + " ms");
                    outputFileName = "parallelSepConv" + img.getName();
                    break;


                // TESTS 4
                case "-debug":
                    System.out.println("Debug");
                    float[][] prova1 = new float[imgHeight][imgWidth * 3];
                    float[][] prova2 = new float[imgHeight][imgWidth * 3];

                    float[][] prova3 = new float[imgHeight][imgWidth * 3];
                    float[][] tmp3 = new float[imgHeight][imgWidth * 3];
                    float[][] prova4 = new float[imgHeight][imgWidth * 3];
                    float[][] tmp4 = new float[imgHeight][imgWidth * 3];

                    Convolution.applyConvolution(pixelMatrix, prova1, imgWidth, imgHeight, 0, imgHeight - 1);
                    Util.testParallelConvolution(pixelMatrix, prova2, imgWidth, imgHeight, NTHREADS);
                    SeparableConvolution.applySeparableConvolution(pixelMatrix, tmp3, prova3, imgWidth, imgHeight, 0, imgHeight - 1, 1);
                    Util.testParallelSepConvolution(pixelMatrix, tmp4, prova4, imgWidth, imgHeight, NTHREADS);
                    System.out.println(Util.checkEqualMatrix(prova1, prova2));
                    System.out.println(Util.checkEqualMatrix(prova2, prova3));
                    System.out.println(Util.checkEqualMatrix(prova3, prova4));
                    if (Util.checkEqualMatrix(prova1, prova2) && Util.checkEqualMatrix(prova2, prova3) && Util.checkEqualMatrix(prova3, prova4)) {
                        System.out.println("Test passed");
                    } else {
                        System.out.println("Test failed");
                    }
                    break;

                default:
                    System.out.println("Option is not present. Please insert the correct option");
                    return;

            }
            if (!args[0].equals("-debug") && !args[0].equals("-h")) {
                String outputPath = args[1].replace(img.getName(), "");
                outputPath = outputPath + outputFileName;
                File output = new File(outputPath);
                BufferedImage outputImage = Util.convertMatrixImageIntoRGBImage(outputMatrix, 3);
                ImageIO.write(outputImage, img.getFormat(), output);
            }


        } catch (IOException | ArrayIndexOutOfBoundsException e) {
            System.out.println("File not found. Please enter a valid input file");
            System.exit(1);
        }
    }

}

