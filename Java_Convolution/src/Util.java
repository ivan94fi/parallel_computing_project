import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Util {

    public static void clearOutputMatrix(float[][] outputMatrix) {
        for (int i = 0; i < outputMatrix.length; i++) {
            for (int j = 0; j < outputMatrix[0].length; j++) {
                outputMatrix[i][j] = 0;
            }
        }
    }

    public static boolean checkEqualMatrix(float[][] mat1, float[][] mat2) {
        for (int i = 0; i < mat1.length; i++) {
            for (int j = 0; j < mat2.length; j++) {
                if (Math.abs(mat1[i][j] - mat2[i][j]) > 0.001) {
                    return false;
                }
            }
        }
        return true;
    }

    public static BufferedImage convertMatrixImageIntoRGBImage(float[][] matrixImage, int numOfChannels) {
        int width = matrixImage[0].length;
        int height = matrixImage.length;
        BufferedImage newImage = new BufferedImage(width / 3, height, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j += numOfChannels) {
                int red = Math.min(Math.max((int) matrixImage[i][j], 0), 255);
                int green = Math.min(Math.max((int) matrixImage[i][j + 1], 0), 255);
                int blue = Math.min(Math.max((int) matrixImage[i][j + 2], 0), 255);
                newImage.setRGB(j / 3, i, new Color(red, green, blue).getRGB());
            }
        }
        return newImage;
    }


    public static void testParallelConvolution(float[][] pixelMatrix, float[][] outputMatrix, int imgWidth, int imgHeight, int NTHREADS) {
        ExecutorService exec = Executors.newFixedThreadPool(NTHREADS - 1);
        final int chunckLen = imgHeight / NTHREADS;
        int offset = 0;
        for (int i = 0; i < NTHREADS - 1; i++) {
            final int startRow = offset;
            final int endRow = offset + chunckLen;
            exec.execute(new Runnable() {
                @Override
                public void run() {
                    Convolution.applyConvolution(pixelMatrix, outputMatrix, imgWidth, imgHeight, startRow, endRow);
                }
            });
            offset += chunckLen + 1;
        }
        Convolution.applyConvolution(pixelMatrix, outputMatrix, imgWidth, imgHeight, pixelMatrix.length - chunckLen, pixelMatrix.length - 1);
        exec.shutdown();

        try {
            if (!exec.awaitTermination(10, TimeUnit.SECONDS)) {
                System.out.println("Time out expired..");
                exec.shutdownNow();
            }

        } catch (InterruptedException ignore) {
            exec.shutdownNow();
            System.exit(1);
        }
    }

    public static void testParallelSepConvolution(float[][] pixelMatrix, float[][] tmpOutputMatrix, float[][] outputMatrix, int imgWidth, int imgHeight, int NTHREADS) {
        ExecutorService exec = Executors.newFixedThreadPool(NTHREADS - 1);
        final int chunckLen = imgHeight / NTHREADS;
        int offset = 0;
        for (int i = 0; i < NTHREADS - 1; i++) {
            final int startRow = offset;
            final int endRow = offset + chunckLen;
            exec.execute(new Runnable() {
                @Override
                public void run() {
                    SeparableConvolution.applySeparableConvolution(pixelMatrix, tmpOutputMatrix, outputMatrix, imgWidth, imgHeight, startRow, endRow, NTHREADS);
                }
            });
            offset += chunckLen + 1;
        }
        SeparableConvolution.applySeparableConvolution(pixelMatrix, tmpOutputMatrix, outputMatrix, imgWidth, imgHeight, imgHeight - chunckLen, imgHeight - 1, NTHREADS);
        exec.shutdown();
        try {
            if (!exec.awaitTermination(10, TimeUnit.SECONDS)) {
                System.out.println("Time out expired..");
                exec.shutdownNow();
            }

        } catch (InterruptedException ignore) {
            exec.shutdownNow();
            System.exit(1);
        }

    }


}
