public class Convolution {

    private static final float[][] kernelMatrix = new float[][]{
            {0.000007f, 0.000425f, 0.001704f, 0.000425f, 0.000007f},
            {0.000425f, 0.027398f, 0.109878f, 0.027398f, 0.000425f},
            {0.001704f, 0.109878f, 0.440655f, 0.109878f, 0.001704f},
            {0.000425f, 0.027398f, 0.109878f, 0.027398f, 0.000425f},
            {0.000007f, 0.000425f, 0.001704f, 0.000425f, 0.000007f}


    };

    private static final int numOfChannels = 3;
    private static final int kernelSize = kernelMatrix.length;
    private static final int maskRadius = (int) Math.floor(kernelSize / 2);

    public static void applyConvolution(float[][] pixelMatrix, float[][] output, int imgWidth, int imgHeight, int startRow, int toRow) {
        try {
            for (int rowImgIdx = startRow; rowImgIdx <= toRow; rowImgIdx++) {
                for (int colImgIdx = 0; colImgIdx < imgWidth * numOfChannels; colImgIdx += numOfChannels) {
                    float sumRed = 0f;
                    float sumBlue = 0f;
                    float sumGreen = 0f;
                    int nStartRow = rowImgIdx - maskRadius; //identify the start row index for convolution.
                    int nStartCol = colImgIdx - maskRadius * numOfChannels; // identify the start col indext for convolution.
                    for (int j = 0; j < kernelSize; j++) {
                        for (int k = 0; k < kernelSize; k++) {
                            int curRow = nStartRow + j; // identify the y coord of the current pixel in the mask.
                            int curCol = nStartCol + k * numOfChannels; // identify the x coord of the current pixel in the mask.
                            if (curRow > -1 && curRow <= toRow + maskRadius && curRow < imgHeight && curCol > -1 && curCol < imgWidth * numOfChannels) {
                                sumRed += pixelMatrix[curRow][curCol] * kernelMatrix[j][k];
                                sumGreen += pixelMatrix[curRow][curCol + 1] * kernelMatrix[j][k];
                                sumBlue += pixelMatrix[curRow][curCol + 2] * kernelMatrix[j][k];
                            }
                        }
                    }
                    output[rowImgIdx][colImgIdx] = sumRed;
                    output[rowImgIdx][colImgIdx + 1] = sumGreen;
                    output[rowImgIdx][colImgIdx + 2] = sumBlue;
                }
            }
        } catch (ArrayIndexOutOfBoundsException o) {
            o.printStackTrace();
            System.exit(1);
        }

    }
}
