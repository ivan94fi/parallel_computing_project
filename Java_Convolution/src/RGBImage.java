import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class RGBImage {
    private static final int numOfChannels = 3;
    private int width;
    private int height;
    private String format;
    private String name;
    private float[][] matrixImage;

    public RGBImage(String directory_path) {
        try {
            File input = new File(directory_path);
            BufferedImage image = ImageIO.read(input);
            this.format = getFileExtension(input);
            this.name = input.getName();
            this.width = image.getWidth();
            this.height = image.getHeight();
            this.matrixImage = new float[this.height][this.width * numOfChannels];
            for (int i = 0; i < this.height; i++) {
                for (int j = 0; j < this.width * numOfChannels; j += 3) {
                    int RGB = image.getRGB(j / 3, i);
                    int r = (RGB >> 16) & 0xff;
                    int g = (RGB >> 8) & 0xff;
                    int b = (RGB) & 0xff;
                    this.matrixImage[i][j] = r;
                    this.matrixImage[i][j + 1] = g;
                    this.matrixImage[i][j + 2] = b;
                }
            }

        } catch (IOException io) {
            System.out.println("File not found. Please enter a valid input file");
            System.exit(1);
        }
    }

    private String getFileExtension(File file) {
        try {
            String name = file.getName();
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (NullPointerException np) {
            np.printStackTrace();
            return "";
        }
    }


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public float[][] getMatrixImage() {
        return matrixImage;
    }

    public String getName() {
        return this.name;
    }

    public String getFormat() {

        return this.format;
    }


}
