import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class SeparableConvolution {


    private static final int numOfChannels = 3;
    private static final float[] kernelRow = {0.002566f, 0.165525f, 0.663818f, 0.165525f, 0.002566f};
    private static final float[] kernelCol = {0.002566f, 0.165525f, 0.663818f, 0.165525f, 0.002566f};

    private static final CyclicBarrier barrier = new CyclicBarrier(Runtime.getRuntime().availableProcessors());
    private static final int kernelSize = kernelRow.length;
    private static final int maskRadius = (int) Math.floor(kernelSize / 2);


    public static void applySeparableConvolution(float[][] pixelMatrix, float[][] tmpOutput, float[][] output, int imgWidth, int imgHeight, int fromRow, int toRow, int NTHREADS) {
        try {
            float sumRed;
            float sumGreen;
            float sumBlue;
            for (int rowImgIdx = fromRow; rowImgIdx <= toRow; rowImgIdx++) {
                for (int colImgIdx = 0; colImgIdx < imgWidth * numOfChannels; colImgIdx += numOfChannels) {
                    sumRed = 0f;
                    sumGreen = 0f;
                    sumBlue = 0f;
                    int nStartCol = colImgIdx - maskRadius * numOfChannels;
                    for (int j = 0; j < kernelSize; j++) {
                        int curCol = nStartCol + j * numOfChannels;
                        if (curCol > -1 && curCol < imgWidth * numOfChannels) {
                            sumRed += pixelMatrix[rowImgIdx][curCol] * kernelRow[j];
                            sumGreen += pixelMatrix[rowImgIdx][curCol + 1] * kernelRow[j];
                            sumBlue += pixelMatrix[rowImgIdx][curCol + 2] * kernelRow[j];
                        }
                    }
                    tmpOutput[rowImgIdx][colImgIdx] = sumRed;
                    tmpOutput[rowImgIdx][colImgIdx + 1] = sumGreen;
                    tmpOutput[rowImgIdx][colImgIdx + 2] = sumBlue;
                }

            }

            if (NTHREADS > 1) {
                barrier.await();
            }

            for (int rowImgIdx = fromRow; rowImgIdx <= toRow; rowImgIdx++) {
                for (int colImgIdx = 0; colImgIdx < imgWidth * numOfChannels; colImgIdx += numOfChannels) {
                    sumRed = 0f;
                    sumGreen = 0f;
                    sumBlue = 0f;
                    int nStartRow = rowImgIdx - maskRadius;
                    for (int k = 0; k < kernelSize; k++) {
                        int curRow = nStartRow + k;
                        if (curRow > -1 && curRow <= toRow + maskRadius && curRow < imgHeight) {
                            sumRed += tmpOutput[curRow][colImgIdx] * kernelCol[k];
                            sumGreen += tmpOutput[curRow][colImgIdx + 1] * kernelCol[k];
                            sumBlue += tmpOutput[curRow][colImgIdx + 2] * kernelCol[k];

                        }
                    }
                    output[rowImgIdx][colImgIdx] = sumRed;
                    output[rowImgIdx][colImgIdx + 1] = sumGreen;
                    output[rowImgIdx][colImgIdx + 2] = sumBlue;
                }
            }
        } catch (BrokenBarrierException | RuntimeException | InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }


    }
}