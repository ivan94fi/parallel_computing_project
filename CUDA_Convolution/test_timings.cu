/*
 * test_timings.cu
 *
 *  Created on: Mar 17, 2018
 *      Author: ivan94fi
 */

#include "test_timings.h"
#include "Image.h"
#include "PPM.h"
#include "convolution.h"
#include "cuda_utils.h"
#include "parameters.h"

#include <cassert>

void test_timings() {

	printf("executing timing tests...\n");

	int image_channels;
	int image_width;
	int image_height;
	Image_t* input_image;
	Image_t* output_image;
	float *host_input_image_data;
	float *device_tmp_image_data;
	float *device_input_image_data;
	float *device_output_image_data;
	float *device_mask_data;

	float *device_separable_row_mask_data;
	float *device_separable_column_mask_data;

	float constant_time;
	float channels_cycle_read_only_time;
	float read_only_time;
	float basic_time;
	float tiled_time;
	float separable_time;

	const int iterations = 16;

	input_image = PPM_import(inputfile);

	image_width = Image_getWidth(input_image);
	image_height = Image_getHeight(input_image);
	image_channels = Image_getChannels(input_image);

	printf("\n");
	printf("Image loaded: %s\n", inputfile);
	printf("width:%d\nheight:%d\nchannels:%d\n", image_width, image_height , image_channels);

	output_image = Image_new(image_width, image_height, image_channels);

	host_input_image_data = Image_getData(input_image);

	float *const_res       = (float*)malloc(sizeof(float) * image_height * image_width * image_channels);
	float *channel_res     = (float*)malloc(sizeof(float) * image_height * image_width * image_channels);
	float *no_channels_res = (float*)malloc(sizeof(float) * image_height * image_width * image_channels);
	float *basic_res       = (float*)malloc(sizeof(float) * image_height * image_width * image_channels);
	float *tiled_res       = (float*)malloc(sizeof(float) * image_height * image_width * image_channels);


	// allocate device buffers
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_input_image_data, sizeof(float) * image_height * image_width * image_channels));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_output_image_data, sizeof(float) * image_height * image_width * image_channels));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_tmp_image_data, sizeof(float) * image_height * image_width * image_channels));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_mask_data, mask_width * mask_width * sizeof(float)));

	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_separable_column_mask_data, mask_width * sizeof(float)));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_separable_row_mask_data, mask_width * sizeof(float)));

	// copy memory from host to device
	CUDA_CHECK_RETURN(cudaMemcpy(device_input_image_data, host_input_image_data,
			image_height * image_width * image_channels * sizeof(float), cudaMemcpyHostToDevice));

	if(!separable) {
		CUDA_CHECK_RETURN(cudaMemcpy(device_mask_data, host_mask_data, mask_width * mask_width * sizeof(float), cudaMemcpyHostToDevice));
		set_constant_mask(host_mask_data); // if separable==true, host_mask_data will be null and therefore cannot be used
	}
	else {
		CUDA_CHECK_RETURN(cudaMemcpy(device_separable_row_mask_data, host_separable_row_mask_data, mask_width * sizeof(float), cudaMemcpyHostToDevice));
		CUDA_CHECK_RETURN(cudaMemcpy(device_separable_column_mask_data, host_separable_column_mask_data, mask_width * sizeof(float), cudaMemcpyHostToDevice));
	}

	int BLOCK_WIDTH = 32;
	dim3 dimGrid(ceil(((float) image_width) / BLOCK_WIDTH),
			ceil(((float) image_height) / BLOCK_WIDTH));
	dim3 dimBlock(BLOCK_WIDTH, BLOCK_WIDTH, 1);

	printf("\nCUDA launch configuration:\n");
	printf("grid:(%d,%d,%d)\nblock:(%d,%d,%d)\n", dimGrid.x, dimGrid.y, dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z);

	printf("\n");
	printf("mask_width: %d\n", mask_width);
	printf("Mask used:\n");
	// tiled kernels: separable and non-separable versions (to be specified via command line options)
	if(separable) {
		printf("separable row kernel: ");
		for(int i=0; i<mask_width; i++)
			printf("%f ", host_separable_row_mask_data[i]);
		printf("\nseparable column kernel: ");
		for(int i=0; i<mask_width; i++)
			printf("%f ", host_separable_column_mask_data[i]);
		printf("\n");

	}
	else {
		for(int i=0; i<mask_width; i++) {
			for(int j=0; j<mask_width; j++) {
				printf("%f ", host_mask_data[i*mask_width+j]);
			}
			printf("\n");
		}
	}


	printf("\n");
	printf("starting tests: averaging on %d iterations\n", iterations);

	/* Read only cache kernel with channels cycle */
	/********************************************************************************************************************/
	for(int i=-1; i<iterations; i++) {
		if(i == 0)
			channels_cycle_read_only_time = 0.0;
		channels_cycle_read_only_time += channels_cycle_read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
					device_output_image_data, image_channels, image_width, image_height, mask_width);
	}

	CUDA_CHECK_RETURN(cudaMemcpy(channel_res, device_output_image_data, sizeof(float) * image_height * image_width * image_channels,
		cudaMemcpyDeviceToHost));

	/* Read only cache memory kernel */
	/********************************************************************************************************************/
	for(int i=-1; i<iterations; i++) {
		if(i == 0)
			read_only_time = 0.0;
		read_only_time += read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
					device_output_image_data, image_channels, image_width, image_height, mask_width);
	}

	CUDA_CHECK_RETURN(cudaMemcpy(no_channels_res, device_output_image_data, sizeof(float) * image_height * image_width * image_channels,
		cudaMemcpyDeviceToHost));


	/********************************************************************************************************************/
	/* Tiled convolution kernel */
	if(separable) {
		for(int i=-1; i<iterations; i++) {
			if(i == 0)
				separable_time = 0.0;
			separable_time += row_separable_read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_separable_row_mask_data,
					device_tmp_image_data, image_channels, image_width, image_height, mask_width);

			CUDA_CHECK_RETURN(cudaDeviceSynchronize());

			separable_time += column_separable_read_only_cache_convolution(dimGrid, dimBlock, device_tmp_image_data, device_separable_column_mask_data,
					device_output_image_data, image_channels, image_width, image_height, mask_width);
		}
	}
	else {
		for(int i=-1; i<iterations; i++) {
			if(i == 0)
				tiled_time = 0.0;
			tiled_time += tiled_read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
					device_output_image_data, image_channels, image_width, image_height, mask_width);
		}
	}

	CUDA_CHECK_RETURN(cudaMemcpy(tiled_res, device_output_image_data, sizeof(float) * image_height * image_width * image_channels,
		cudaMemcpyDeviceToHost));

	/********************************************************************************************************************/
	/* Constant memory kernel */
	if(!separable) {
		for(int i=-1; i<iterations; i++) {
			if(i == 0)
				constant_time = 0.0;
			constant_time += constant_memory_convolution(dimGrid, dimBlock, device_input_image_data,
						device_output_image_data, image_channels, image_width, image_height);
		}
	}

	CUDA_CHECK_RETURN(cudaMemcpy(const_res, device_output_image_data, sizeof(float) * image_height * image_width * image_channels,
		cudaMemcpyDeviceToHost));

	/********************************************************************************************************************/
	/* Basic convolution kernel */
	for(int i=-1; i<iterations; i++) {
		if(i == 0)
			basic_time = 0.0;
		basic_time += basic_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
				device_output_image_data, image_channels, image_width, image_height, mask_width);
	}

	CUDA_CHECK_RETURN(cudaMemcpy(basic_res, device_output_image_data, sizeof(float) * image_height * image_width * image_channels,
		cudaMemcpyDeviceToHost));


    printf("basic time: %f ms\n", basic_time / iterations);
	printf("no channels cycle read-only cache: %f ms\n", read_only_time / iterations);
	printf("channels cycle read-only cache: %f ms\n", channels_cycle_read_only_time / iterations);
	if(separable)
		printf("separable tiled time: %f ms\n", separable_time / iterations);
	else
		printf("tiled time: %f ms\n", tiled_time / iterations);

	if(!separable)
		printf("constant memory kernel time: %f ms\n", constant_time / iterations);

	// free device memory
	cudaFree(device_input_image_data);
	cudaFree(device_output_image_data);
	cudaFree(device_mask_data);

	// free host memory
	Image_delete(output_image);
	Image_delete(input_image);
	free(host_mask_data);
	free(const_res);
	free(channel_res);
	free(no_channels_res);
	free(basic_res);
	free(tiled_res);

	printf("\nexecution terminated without errors.\n");
}
