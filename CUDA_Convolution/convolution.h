/*
 * convolution.h
 *
 *  Created on: Mar 13, 2018
 *      Author: ivan94fi
 */
#include "parameters.h"


#ifndef CONVOLUTION_H_
#define CONVOLUTION_H_

extern "C" float read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width);

extern "C" float channels_cycle_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width);


extern "C" float constant_memory_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		float *P,
		int channels, int width, int height);

extern "C" float basic_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		float *M,
		float *P,
		int channels, int width, int height, int mask_width);

extern "C" float tiled_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width);

extern "C" float row_separable_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width);

extern "C" float column_separable_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width);

extern "C" void set_constant_mask(float *host_constant_mask_data);


#endif /* CONVOLUTION_H_ */
