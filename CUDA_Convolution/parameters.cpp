#include "parameters.h"
#include <iostream>
#include <sstream>
#include <random>
#include <chrono>
#include <getopt.h>
#include <cstring>
#include <cmath>


const char *inputfile;
const char *outputfile;
float blur = 0;
float sigma = 0;
int edge = 0;
int emboss = 0;

int write = 1;
int timings = 0;
int separable = 0;
int help = 0;

int mask_width = DEFAULT_MASK_WIDTH;
int mask_radius = DEFAULT_MASK_RADIUS;

float *host_mask_data;
float *host_separable_row_mask_data;
float *host_separable_column_mask_data;

float emboss_kernel[DEFAULT_MASK_WIDTH * DEFAULT_MASK_WIDTH] = {  //    EMBOSS KERNEL
		0, 0, 0, 0, 0,
		0,-2,-1, 0, 0,
		0,-1, 1, 1, 0,
		0, 0, 1, 2,	0,
		0, 0, 0, 0, 0,
};

float sobel_kernel[9 * 9] = {
        -4.0/32, -3.0/25, -2.0/20, -1.0/17,  0.0/16, 1.0/17, 2.0/20,  3.0/25,  4.0/32,
        -4.0/25, -3.0/18, -2.0/13, -1.0/10,  0.0/9,  1.0/10, 2.0/13,  3.0/18,  4.0/25,
        -4.0/20, -3.0/13, -2.0/8,  -1.0/5,   0.0/4,  1.0/5,  2.0/8,   3.0/13,  4.0/20,
        -4.0/17, -3.0/10, -2.0/5,  -1.0/2,   0.0/1,  1.0/2,  2.0/5,   3.0/10,  4.0/17,
        -4.0/16, -3.0/9,  -2.0/4,  -1.0/1,   0.0,    1.0/1,  2.0/4,   3.0/9,   4.0/16,
        -4.0/17, -3.0/10, -2.0/5,  -1.0/2,   0.0/1,  1.0/2,  2.0/5,   3.0/10,  4.0/17,
        -4.0/20, -3.0/13, -2.0/8,  -1.0/5,   0.0/4,  1.0/5,  2.0/8,   3.0/13,  4.0/20,
        -4.0/25, -3.0/18, -2.0/13, -1.0/10,  0.0/9,  1.0/10, 2.0/13,  3.0/18,  4.0/25,
        -4.0/32, -3.0/25, -2.0/20, -1.0/17,  0.0/16, 1.0/17, 2.0/20,  3.0/25,  4.0/32,
};

void get_cmd_opt(int argc, char *argv[]) {

	int c;
	static struct option long_options[] =
	{
			/* flag options */
			{"emboss", no_argument , &emboss, 1},
			{"nowrite", no_argument, &write, 0},
			{"timings", no_argument, &timings, 1},
			{"separable", no_argument, &separable, 1},
			{"help", no_argument, &help, 1},
			/* non-flag options */
			{"blur", required_argument, nullptr, 'b'},
			{"gaussian", required_argument, nullptr, 'g'},
			{"edge", required_argument, nullptr, 'e'},
			{0, 0, 0, 0}
	};

	std::stringstream ssopt;

	/* before checking other options look for --separable/-s. */
	for(int i=1; i<argc; i++) {
		if(strcmp(argv[i],"--separable") == 0 || strcmp(argv[i],"-s") == 0) {
			separable = 1;
			printf("-option separable\n");
			break;
		}
	}

	while (true) {
		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long(argc, argv, "b:g:e:h",
				long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c) {
		case 0:
			/* If this option set a flag, do nothing else now. */
			if (long_options[option_index].flag != nullptr)
				break;
			printf("-option %s", long_options[option_index].name);
			if (optarg)
				printf(" with arg %s", optarg);
			printf("\n");
			break;

		case 'b':
		{
			ssopt.str(optarg);
			ssopt >> blur;
			ssopt.clear();
			if(blur == 1) {
				printf("-option blur kernel: value 1\n");
				mask_width = 5;
			}
			else if(blur == 2) {
				printf("-option blur kernel: value 2\n");
				mask_width = 7;
			}
			else if(blur == 3) {
				printf("-option blur kernel: value 3\n");
				mask_width = 9;
			}
			else {
				printf("-option blur kernel: default value\n");
				mask_width = 5;
			}
			if(separable) {
				host_separable_row_mask_data = (float*)malloc(sizeof(float) * mask_width);
				host_separable_column_mask_data = (float*)malloc(sizeof(float) * mask_width);
				float val = 1.0/mask_width;
				for(int i=0; i<mask_width; i++) {
					host_separable_row_mask_data[i] = val;
					host_separable_column_mask_data[i] = val;
				}
			}
			else {
				host_mask_data = (float*)malloc(sizeof(float) * mask_width * mask_width);
				float val = 1.0/(mask_width*mask_width);
				for(int i=0; i<mask_width*mask_width; i++)
					host_mask_data[i] = val;
			}
			break;
		}

		case 'g':
			ssopt.str(optarg);
			ssopt >> sigma;
			ssopt.clear();
			if(separable)
				generate_gaussian_kernel_1D(&host_separable_row_mask_data, &host_separable_column_mask_data, sigma);
			else
				generate_gaussian_kernel_2D(&host_mask_data, sigma);
			printf("-option gaussian blur: sigma=%f\n", sigma);
			break;

		case 'e':
			ssopt.str(optarg);
			ssopt >> edge;
			ssopt.clear();
			if(edge == 1) {
				printf("-option edge detect kernel: value 1\n");
				mask_width = 5;
			}
			else if(edge == 2) {
				printf("-option edge detect kernel: value 2\n");
				mask_width = 7;
			}
			else if(edge == 3) {
				printf("-option edge detect kernel: value 3\n");
				mask_width = 9;
			}
			else {
				printf("-option edge detect kernel: default value\n");
				mask_width = 5;
			}
			if(separable) {
				host_separable_row_mask_data = (float*)malloc(sizeof(float) * mask_width);
				host_separable_column_mask_data = (float*)malloc(sizeof(float) * mask_width);
				generate_edge_detect_kernel_1D(host_separable_row_mask_data, host_separable_column_mask_data, mask_width);
			}
			else {
				host_mask_data = (float*)malloc(sizeof(float)* mask_width * mask_width);
				generate_edge_detect_kernel_2D(host_mask_data, mask_width);
			}

			break;

		case 'h':
			display_help();
			exit(0);
			break;
		case '?':
			/* getopt_long already printed an error message. */
			display_usage();
			exit(0);
			break;

		default:
			abort();
		}
	}

	/* handle flags args*/
	if(help == 1) {
		display_help();
		exit(0);
	}
	if (emboss == 1) {
		printf("-option emboss\n");
		separable = 0;
		host_mask_data = (float*)malloc(sizeof(float)*mask_width*mask_width);
		for(int i=0; i<mask_width*mask_width; i++)
			host_mask_data[i] = emboss_kernel[i];
	}
	if(write == 0)
		printf("-option no write\n");
	if(timings == 1)
		printf("-option timings: executing timing tests\n");

	/* handle non-option args */
	if (optind < argc)
	{
		if(argv[optind]) {
			inputfile = argv[optind];
			printf("-option inputfile: %s\n", inputfile);
		}

		optind++;
		if(argv[optind]) {
			outputfile = argv[optind];
			printf("-option outputfile: %s\n", outputfile);
		}
		else {
			const char *pch=strrchr(inputfile,'.');
			int pos = (int)(pch-inputfile);
			int len = (int)strlen(inputfile);
			char *tmpout = (char*)calloc((len + 13), sizeof(char));
			strncpy(tmpout, inputfile, pos);
			strcat(tmpout, "_conv_out.ppm");
			outputfile = tmpout;
			printf("-option outputfile not provided. Default name: %s.\n", outputfile);

		}
	}
	else {
		if(write)
			printf("-option filename not provided. Using the default image.\n");
		inputfile = "images/computer_programming.ppm";
		outputfile = "images/computer_programming_conv_out.ppm";
	}

	if(!host_mask_data && !host_separable_row_mask_data && !host_separable_column_mask_data) {
			printf("-no filter specified: using the default constant blur kernel.\n");
			// Default blur kernel
			if(separable) {
				host_separable_row_mask_data = (float*)malloc(sizeof(float) * mask_width);
				host_separable_column_mask_data = (float*)malloc(sizeof(float) * mask_width);
				float val = 1.0/mask_width;
				for(int i=0; i<mask_width; i++) {
					host_separable_row_mask_data[i] = val;
					host_separable_column_mask_data[i] = val;
				}
			}
			else {
				host_mask_data = (float*)malloc(sizeof(float) * mask_width * mask_width);
				float val = 1.0/(mask_width*mask_width);
				for(int i=0; i<mask_width*mask_width; i++)
					host_mask_data[i] = val;
			}
	}
	printf("\n\n");
}

void generate_gaussian_kernel_1D(float **row, float **col, float sigma) {
	int i;
	mask_radius = ceil(3 * sigma);
	mask_width = 2 * mask_radius + 1;
	int mu = mask_radius;
	(*row) = (float*)malloc(sizeof(float) * mask_width);
	(*col) = (float*)malloc(sizeof(float) * mask_width);
	//*col = *row;
	float sum = 0.0;
	float val;
	for(int i=0; i<mask_width; i++) {
		val = exp( (i-mu)*(i-mu) / (-2.0 * sigma * sigma) );
		sum += val;
		(*row)[i] = val;
		(*col)[i] = val;
	}

	// Normalize the kernel.
	for (i=0; i<mask_width; i++) {
        (*row)[i] /= sum;
        (*col)[i] /= sum;
	}

	return;
}


void generate_gaussian_kernel_2D(float **mask, float sigma) {
	int i, j;
	mask_radius = ceil(3 * sigma);
	mask_width = 2 * mask_radius +1;
	int mu = mask_radius;
	(*mask) = (float*)malloc(sizeof(float) * mask_width * mask_width);
	float sum = 0.0;
	float val;
	for(int i=0; i<mask_width; i++) {
		for(int j=0; j<mask_width; j++) {
			val = exp( ((i-mu)*(i-mu) + (j-mu)*(j-mu))  /  (-2.0 * sigma * sigma)
					 );
			sum += val;
			(*mask)[i*mask_width + j] = val;
		}
	}

	// Normalize the kernel.
	for (i=0; i<mask_width; i++)
	    for (j=0; j<mask_width; j++)
	        (*mask)[i*mask_width + j] /= sum;

	return;
}

void generate_edge_detect_kernel_2D(float *mask, int mask_width) {
	int offset = (9 - mask_width) / 2;
	for(int i=0; i<mask_width; i++)
		for(int j=0; j<mask_width; j++)
			host_mask_data[i*mask_width+j] = sobel_kernel[(i+offset)*9 + (j+offset)];
}

void generate_edge_detect_kernel_1D(float *row, float *col, int mask_width) {
	int offset = (9 - mask_width) / 2;
	for(int i=0; i<mask_width; i++)
		for(int j=0; j<mask_width; j++)
			host_mask_data[i*mask_width+j] = sobel_kernel[(i+offset)*9 + (j+offset)];
}

void display_help() {
	printf("CUDA Convolution: apply convolution to an input image in ppm format. It is possible to choose between various kernels. If no output filename is specified the program appends '_conv_out' to the input filename.\n");
	display_usage();
}

void display_usage() {
    printf("Usage: CUDA_Convolution [[OPTION] [VALUE]] [INPUTFILE] [OUTPUTFILE]\n");

    printf("\n");

    printf("  -b VALUE, --blur VALUE           use uniform blur: possible values are 1, 2 and 3.\n");
    printf("  -g STDDEV, --gauss STDDEV        use gaussian blur with standard deviation STDDEV (may be a real number).\n");
    printf("  -e VALUE, --edge VALUE           use an edge detect (Sobel-like) kernel: possible values are 1, 2 and 3.\n");

    printf("  --emboss                         use an embossing kernel (--separable has no effect).\n");
    printf("  --timings                        test all cuda kernels and print timings.\n");
    printf("  --separable                      use a separable kernel approach.\n");
    printf("  -h, --help                       print an help message.\n");
}
