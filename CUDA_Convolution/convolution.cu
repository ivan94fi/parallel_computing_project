#include "convolution.h"

#include <cassert>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_functions.h>
#include <device_launch_parameters.h>
#include <driver_types.h>
#include <math_functions.h>
#include <vector_types.h>

#include "cuda_utils.h"
#include "parameters.h"


__constant__ float constant_device_mask_data[DEFAULT_MASK_WIDTH * DEFAULT_MASK_WIDTH];

extern "C" void set_constant_mask(float *host_constant_mask_data) {
	CUDA_CHECK_RETURN(cudaMemcpyToSymbol(constant_device_mask_data, host_constant_mask_data, DEFAULT_MASK_WIDTH * DEFAULT_MASK_WIDTH * sizeof(float)));
}


__global__ void read_only_cache_kernel(float *I, const float *__restrict__ M, float *P, int channels, int width, int height, int mask_width) {

	const int mask_radius = mask_width / 2;
	int row = (blockIdx.y * blockDim.y + threadIdx.y);
	int col = (blockIdx.x * blockDim.x + threadIdx.x) * channels;

	if (col < (width * channels) && row < height) {
		float pix_val_R = 0;
		float pix_val_G = 0;
		float pix_val_B = 0;

		int N_start_row = row - (mask_radius);
		int N_start_col = col - (mask_radius) * channels;

		// Get the elements of the surrounding box
		for(int j = 0; j < mask_width; j++) {
			for(int k = 0; k < mask_width; k++) {
				int cur_row = N_start_row + j;
				int cur_col = N_start_col + k * channels;
				// Verify we have a valid image pixel
				if(cur_row > -1 && cur_row < height && cur_col > -1 && cur_col < width * channels) {
					pix_val_R += I[cur_row * width * channels + cur_col + 0] * M[j*mask_width+k];
					pix_val_G += I[cur_row * width * channels + cur_col + 1] * M[j*mask_width+k];
					pix_val_B += I[cur_row * width * channels + cur_col + 2] * M[j*mask_width+k];
				}
			}
		}
		// Write the pixel value
		P[row * width * channels + col + 0] = clamp(pix_val_R);
		P[row * width * channels + col + 1] = clamp(pix_val_G);
		P[row * width * channels + col + 2] = clamp(pix_val_B);
	}
}

extern "C" float read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width) {

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	read_only_cache_kernel<<<dim_grid, dim_block>>>(I, M, P, channels, width, height, mask_width);
	cudaEventRecord(stop);

	CUDA_CHECK_ERROR();

	cudaEventSynchronize(stop);

	float elapsed_ms;
	cudaEventElapsedTime(&elapsed_ms, start, stop);

	return elapsed_ms;
}

__global__ void channels_cycle_read_only_cache_kernel(float *I, const float *__restrict__ M, float *P, int channels, int width, int height, int mask_width) {

	const int mask_radius = mask_width / 2;
	for(int c=0; c<channels; c++) {

		int row = (blockIdx.y * blockDim.y + threadIdx.y);
		int col = (blockIdx.x * blockDim.x + threadIdx.x) * channels + c;

		if (col < (width * channels) && row < height) {
			float pixVal = 0;

			int N_start_row = row - (mask_radius);
			int N_start_col = col - (mask_radius) * channels;

			// Get the elements of the surrounding box
			for(int j = 0; j < mask_width; j++) {
				for(int k = 0; k < mask_width; k++) {
					int cur_row = N_start_row + j;
					int cur_col = N_start_col + k * channels;
					// Verify we have a valid image pixel
					if(cur_row > -1 && cur_row < height && cur_col > -1 && cur_col < width * channels)
						pixVal += I[cur_row * width * channels + cur_col + c] * M[j*mask_width+k];
				}
			}
			// Write the pixel value
			P[row * width * channels + col + c] = clamp(pixVal);
		}
	}
}

extern "C" float channels_cycle_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width) {

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	channels_cycle_read_only_cache_kernel<<<dim_grid, dim_block>>>(I, M, P, channels, width, height, mask_width);
	cudaEventRecord(stop);

	CUDA_CHECK_ERROR();

	cudaEventSynchronize(stop);

	float elapsed_ms;
	cudaEventElapsedTime(&elapsed_ms, start, stop);

	return elapsed_ms;
}


__global__ void constant_memory_kernel(float *I, float *P, int channels, int width, int height) {

	int row = (blockIdx.y * blockDim.y + threadIdx.y);
	int col = (blockIdx.x * blockDim.x + threadIdx.x) * channels;

	if (col < (width * channels) && row < height) {
		float pix_val_R = 0;
		float pix_val_G = 0;
		float pix_val_B = 0;

		int N_start_row = row - (DEFAULT_MASK_RADIUS);
		int N_start_col = col - (DEFAULT_MASK_RADIUS) * channels;

		// Get the elements of the surrounding box
		for(int j = 0; j < DEFAULT_MASK_WIDTH; j++) {
			for(int k = 0; k < DEFAULT_MASK_WIDTH; k++) {
				int cur_row = N_start_row + j;
				int cur_col = N_start_col + k * channels;
				// Verify we have a valid image pixel
				if(cur_row > -1 && cur_row < height && cur_col > -1 && cur_col < width * channels) {
					pix_val_R += I[cur_row * width * channels + cur_col + 0] * constant_device_mask_data[j*DEFAULT_MASK_WIDTH+k];
					pix_val_G += I[cur_row * width * channels + cur_col + 1] * constant_device_mask_data[j*DEFAULT_MASK_WIDTH+k];
					pix_val_B += I[cur_row * width * channels + cur_col + 2] * constant_device_mask_data[j*DEFAULT_MASK_WIDTH+k];
				}
			}
		}
		// Write the pixel value
		P[row * width * channels + col + 0] = clamp(pix_val_R);
		P[row * width * channels + col + 1] = clamp(pix_val_G);
		P[row * width * channels + col + 2] = clamp(pix_val_B);
	}
}

extern "C" float constant_memory_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		float *P,
		int channels, int width, int height) {

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	constant_memory_kernel<<<dim_grid, dim_block>>>(I, P, channels, width, height);
	cudaEventRecord(stop);

	CUDA_CHECK_ERROR();

	cudaEventSynchronize(stop);

	float elapsed_ms;
	cudaEventElapsedTime(&elapsed_ms, start, stop);

	return elapsed_ms;
}


__global__ void basic_kernel(float *I, float *M, float *P, int channels, int width, int height, int mask_width) {

	const int mask_radius = mask_width / 2;
	int row = (blockIdx.y * blockDim.y + threadIdx.y);
	int col = (blockIdx.x * blockDim.x + threadIdx.x) * channels;

	if (col < (width * channels) && row < height) {
		float pix_val_R = 0;
		float pix_val_G = 0;
		float pix_val_B = 0;

		int N_start_row = row - (mask_radius);
		int N_start_col = col - (mask_radius) * channels;

		// Get the elements of the surrounding box
		for(int j = 0; j < mask_width; j++) {
			for(int k = 0; k < mask_width; k++) {
				int cur_row = N_start_row + j;
				int cur_col = N_start_col + k * channels;
				// Verify we have a valid image pixel
				if(cur_row > -1 && cur_row < height && cur_col > -1 && cur_col < width * channels) {
					pix_val_R += I[cur_row * width * channels + cur_col + 0] * M[j*mask_width+k];
					pix_val_G += I[cur_row * width * channels + cur_col + 1] * M[j*mask_width+k];
					pix_val_B += I[cur_row * width * channels + cur_col + 2] * M[j*mask_width+k];
				}
			}
		}
		// Write the pixel value
		P[row * width * channels + col + 0] = clamp(pix_val_R);
		P[row * width * channels + col + 1] = clamp(pix_val_G);
		P[row * width * channels + col + 2] = clamp(pix_val_B);
	}
}



extern "C" float basic_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		float *M,
		float *P,
		int channels, int width, int height, int mask_width) {

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	basic_kernel<<<dim_grid, dim_block>>>(I, M, P, channels, width, height, mask_width);
	cudaEventRecord(stop);

	CUDA_CHECK_ERROR();

	cudaEventSynchronize(stop);

	float elapsed_ms;
	cudaEventElapsedTime(&elapsed_ms, start, stop);

	return elapsed_ms;

}

__global__ void tiled_read_only_cache_kernel(float *I, const float *__restrict__ M, float *P, int channels, int width, int height, int mask_width, int w) {
	const int mask_radius = mask_width / 2;
	const int TILE_WIDTH = blockDim.x;

	extern __shared__ float s_M[];

	// First batch loading
	int dst = (threadIdx.y * TILE_WIDTH  + threadIdx.x) * channels;
	int dst_y = dst / (w * channels);
	int dst_x = dst % (w * channels);
	int src_x = blockIdx.x * (TILE_WIDTH * channels) + dst_x - mask_radius * channels;
	int src_y = blockIdx.y * TILE_WIDTH + dst_y - mask_radius;
	int src = (src_y * width * channels) + src_x;
	if(src_y > -1 && src_y < height && src_x > -1 && src_x < width * channels) {
		s_M[dst_y * w * channels + dst_x + 0] = I[src + 0];
		s_M[dst_y * w * channels + dst_x + 1] = I[src + 1];
		s_M[dst_y * w * channels + dst_x + 2] = I[src + 2];
	} else {
		s_M[dst_y * w * channels + dst_x + 0] = 0.0;
		s_M[dst_y * w * channels + dst_x + 1] = 0.0;
		s_M[dst_y * w * channels + dst_x + 2] = 0.0;
	}

	// Second batch loading
	dst = (threadIdx.y * TILE_WIDTH + threadIdx.x + TILE_WIDTH * TILE_WIDTH) * channels;
	dst_y = dst / (w * channels);
	dst_x = dst % (w * channels);
	src_x = blockIdx.x * (TILE_WIDTH * channels) + dst_x - mask_radius * channels;
	src_y = blockIdx.y * TILE_WIDTH + dst_y - mask_radius;
	src = (src_y * width * channels) + src_x;
	if(dst_y < w) {
		if(src_y > -1 && src_y < height && src_x > -1 && src_x < width * channels) {
			s_M[dst_y * w * channels + dst_x + 0] = I[src + 0];
			s_M[dst_y * w * channels + dst_x + 1] = I[src + 1];
			s_M[dst_y * w * channels + dst_x + 2] = I[src + 2];
		} else {
			s_M[dst_y * w * channels + dst_x + 0] = 0.0;
			s_M[dst_y * w * channels + dst_x + 1] = 0.0;
			s_M[dst_y * w * channels + dst_x + 2] = 0.0;
		}
	}

	__syncthreads();

	// Begin calculation of output values.
	float pix_val_R = 0.0;
	float pix_val_G = 0.0;
	float pix_val_B = 0.0;

	int x, y;

	for(y=0; y < mask_width; y++) {
		for(x=0; x < mask_width; x++) {
			pix_val_R += s_M[(((threadIdx.y + y) * w)+(threadIdx.x + x)) * channels + 0] * M[y*mask_width + x];
			pix_val_G += s_M[(((threadIdx.y + y) * w)+(threadIdx.x + x)) * channels + 1] * M[y*mask_width + x];
			pix_val_B += s_M[(((threadIdx.y + y) * w)+(threadIdx.x + x)) * channels + 2] * M[y*mask_width + x];
		}
	}

	// Calculate index in global memory matrix.
	x = (blockIdx.x * TILE_WIDTH  + threadIdx.x) * channels;
	y = blockIdx.y * TILE_WIDTH + threadIdx.y;

	if(y < height && x < width * channels) {
		P[y * width * channels + x + 0] = clamp(pix_val_R);
		P[y * width * channels + x + 1] = clamp(pix_val_G);
		P[y * width * channels + x + 2] = clamp(pix_val_B);
	}
}



extern "C" float tiled_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block,
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width) {

	int w = dim_block.x + mask_width - 1;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	tiled_read_only_cache_kernel<<<dim_grid, dim_block, sizeof(float) * (w * w* 3)>>>(I, M, P, channels, width, height, mask_width, w);
	cudaEventRecord(stop);

	CUDA_CHECK_ERROR();

	cudaEventSynchronize(stop);

	float elapsed_ms;
	cudaEventElapsedTime(&elapsed_ms, start, stop);

	return elapsed_ms;
}


__global__ void row_separable_read_only_cache_kernel(float *I, const float *__restrict__ M, float *P, int channels, int width, int height, int mask_width) {

	const int mask_radius = mask_width / 2;
	const int TILE_WIDTH = blockDim.x;
	extern __shared__ float s_I[];

	const int w = TILE_WIDTH + mask_width - 1;

	// case1: left
	int s_coord = (threadIdx.y * TILE_WIDTH + threadIdx.x) * channels;
	int s_row = s_coord / (w * channels);
	int s_col = s_coord % (w * channels);

	int row = blockIdx.y * blockDim.y + s_row;
	int col = blockIdx.x * (blockDim.x * channels) + s_col - mask_radius * channels;
	int coord = (row * width * channels) + col;

	if(row > -1 && row < height && col > -1 && col < width * channels) {
		s_I[s_row * w * channels + s_col + 0] = I[coord + 0];
		s_I[s_row * w * channels + s_col + 1] = I[coord + 1];
		s_I[s_row * w * channels + s_col + 2] = I[coord + 2];
	}
	else {
		s_I[s_row * w * channels + s_col + 0] = 0.0;
		s_I[s_row * w * channels + s_col + 1] = 0.0;
		s_I[s_row * w * channels + s_col + 2] = 0.0;
	}

	// case 2: right
	s_coord = (threadIdx.y * TILE_WIDTH + threadIdx.x + TILE_WIDTH * TILE_WIDTH) * channels;
	s_row = s_coord / (w * channels);
	s_col = s_coord % (w * channels);

	row = blockIdx.y * blockDim.y + s_row;
	col = blockIdx.x * (blockDim.x * channels) + s_col - mask_radius * channels;
	coord = (row * width * channels) + col;

	if(s_row < TILE_WIDTH) {
		if(row > -1 && row < height && col > -1 && col < width * channels) {
			s_I[s_row * w * channels + s_col + 0] = I[coord + 0];
			s_I[s_row * w * channels + s_col + 1] = I[coord + 1];
			s_I[s_row * w * channels + s_col + 2] = I[coord + 2];
		}
		else {
			s_I[s_row * w * channels + s_col + 0] = 0.0;
			s_I[s_row * w * channels + s_col + 1] = 0.0;
			s_I[s_row * w * channels + s_col + 2] = 0.0;
		}
	}


	__syncthreads();

	// Begin calculation of output values.
	float pix_val_R = 0.0;
	float pix_val_G = 0.0;
	float pix_val_B = 0.0;

	for(int i=-mask_radius; i<=mask_radius; i++) {
		pix_val_R += s_I[((threadIdx.y * w) + (threadIdx.x + mask_radius + i)) * channels + 0] * M[i + mask_radius];
		pix_val_G += s_I[((threadIdx.y * w) + (threadIdx.x + mask_radius + i)) * channels + 1] * M[i + mask_radius];
		pix_val_B += s_I[((threadIdx.y * w) + (threadIdx.x + mask_radius + i)) * channels + 2] * M[i + mask_radius];
	}

	row = blockIdx.y * blockDim.y + threadIdx.y;
	col = (blockIdx.x * blockDim.x + threadIdx.x) * channels;

	if(row < height && col < width * channels) {
		P[row * width * channels + col + 0] = clamp(pix_val_R);
		P[row * width * channels + col + 1] = clamp(pix_val_G);
		P[row * width * channels + col + 2] = clamp(pix_val_B);
	}
}


extern "C" float row_separable_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block, /*int dim_shared,*/
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width) {

	const int TILE_WIDTH = dim_block.x;
	int s_dim = TILE_WIDTH * (TILE_WIDTH + mask_width - 1) * channels;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	row_separable_read_only_cache_kernel<<<dim_grid, dim_block, sizeof(float) * s_dim>>>(I, M, P, channels, width, height, mask_width);
	cudaEventRecord(stop);

	CUDA_CHECK_ERROR();

	cudaEventSynchronize(stop);

	float elapsed_ms;
	cudaEventElapsedTime(&elapsed_ms, start, stop);

	return elapsed_ms;
}

__global__ void column_separable_read_only_cache_kernel(float *I, const float *__restrict__ M, float *P, int channels, int width, int height, int mask_width) {
	const int mask_radius = mask_width / 2;

		extern __shared__ float s_I[];
		const int TILE_WIDTH = blockDim.x;
		const int w = TILE_WIDTH + mask_width - 1;

		int s_coord = (threadIdx.y * TILE_WIDTH + threadIdx.x) * channels;
		int s_row = s_coord / (TILE_WIDTH * channels);
		int s_col = s_coord % (TILE_WIDTH * channels);

		int row = blockIdx.y * blockDim.y + s_row - mask_radius;
		int col = blockIdx.x * (blockDim.x * channels) + s_col;
		int coord = (row * width * channels) + col;

		if(row > -1 && row < height && col > -1 && col < width * channels) {
			s_I[s_row * TILE_WIDTH * channels + s_col + 0] = I[coord + 0];
			s_I[s_row * TILE_WIDTH * channels + s_col + 1] = I[coord + 1];
			s_I[s_row * TILE_WIDTH * channels + s_col + 2] = I[coord + 2];
		}
		else {
			s_I[s_row * TILE_WIDTH * channels + s_col + 0] = 0.0;
			s_I[s_row * TILE_WIDTH * channels + s_col + 1] = 0.0;
			s_I[s_row * TILE_WIDTH * channels + s_col + 2] = 0.0;
		}

		// case2: lower
		s_coord = (threadIdx.y * TILE_WIDTH + threadIdx.x + TILE_WIDTH * TILE_WIDTH) * channels;
		s_row = s_coord / (TILE_WIDTH * channels);
		s_col = s_coord % (TILE_WIDTH * channels);

		row = blockIdx.y * blockDim.y + s_row - mask_radius;
		col = blockIdx.x * (blockDim.x * channels) + s_col;
		coord = (row * width * channels) + col;

		if(s_row < w) {
			if(row > -1 && row < height && col > -1 && col < width * channels) {
				s_I[s_row * TILE_WIDTH * channels + s_col + 0] = I[coord + 0];
				s_I[s_row * TILE_WIDTH * channels + s_col + 1] = I[coord + 1];
				s_I[s_row * TILE_WIDTH * channels + s_col + 2] = I[coord + 2];
			}
			else {
				s_I[s_row * TILE_WIDTH * channels + s_col + 0] = 0.0;
				s_I[s_row * TILE_WIDTH * channels + s_col + 1] = 0.0;
				s_I[s_row * TILE_WIDTH * channels + s_col + 2] = 0.0;
			}
		}

		__syncthreads();

		// Begin calculation of output values.
		float pix_val_R = 0.0;
		float pix_val_G = 0.0;
		float pix_val_B = 0.0;

		for(int i=-mask_radius; i<=mask_radius; i++) {
			pix_val_R += s_I[((threadIdx.y + i + mask_radius) * TILE_WIDTH + threadIdx.x) * channels + 0] * M[i + mask_radius];
			pix_val_G += s_I[((threadIdx.y + i + mask_radius) * TILE_WIDTH + threadIdx.x) * channels + 1] * M[i + mask_radius];
			pix_val_B += s_I[((threadIdx.y + i + mask_radius) * TILE_WIDTH + threadIdx.x) * channels + 2] * M[i + mask_radius];
		}

		row = blockIdx.y * blockDim.y + threadIdx.y;
		col = (blockIdx.x * blockDim.x + threadIdx.x) * channels;

		if(row < height && col < width * channels) {
			P[row * width * channels + col + 0] = clamp(pix_val_R);
			P[row * width * channels + col + 1] = clamp(pix_val_G);
			P[row * width * channels + col + 2] = clamp(pix_val_B);
		}

}

extern "C" float column_separable_read_only_cache_convolution(
		dim3 dim_grid, dim3 dim_block, /*int dim_shared,*/
		float *I,
		const float *__restrict__ M,
		float *P,
		int channels, int width, int height, int mask_width) {

	const int TILE_WIDTH = dim_block.x;
	int s_dim = TILE_WIDTH * (TILE_WIDTH + mask_width - 1) * channels;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	column_separable_read_only_cache_kernel<<<dim_grid, dim_block, sizeof(float) * s_dim>>>(I, M, P, channels, width, height, mask_width);
	cudaEventRecord(stop);

	CUDA_CHECK_ERROR();

	cudaEventSynchronize(stop);

	float elapsed_ms;
	cudaEventElapsedTime(&elapsed_ms, start, stop);

	return elapsed_ms;

}

