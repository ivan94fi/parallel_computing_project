#include "test_timings.h"
#include "test_convolution.h"
#include "parameters.h"

int main(int argc, char *argv[]) {

	get_cmd_opt(argc, argv);

	if(timings)
		test_timings();
	else
		test_convolution();

	return 0;
}
