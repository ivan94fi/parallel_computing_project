/*
 * test_convolution.cu
 *
 *  Created on: Mar 17, 2018
 *      Author: ivan94fi
 */

#include "test_convolution.h"
#include "Image.h"
#include "PPM.h"
#include "convolution.h"
#include "cuda_utils.h"
#include "parameters.h"



void test_convolution() {

	printf("executing single convolution test...\n");

	int image_channels;
	int image_width;
	int image_height;
	Image_t* input_image;
	Image_t* output_image;
	float *host_input_image_data;
	float *host_output_image_data;
	float *device_input_image_data;
	float *device_tmp_image_data;
	float *device_output_image_data;
	float *device_mask_data;
	float elapsed_time;


	float *device_separable_row_mask_data;
	float *device_separable_column_mask_data;

	input_image = PPM_import(inputfile);

	image_width = Image_getWidth(input_image);
	image_height = Image_getHeight(input_image);
	image_channels = Image_getChannels(input_image);

	printf("\n");
	printf("Image loaded: %s\n", inputfile);
	printf("width:%d\nheight:%d\nchannels:%d\n", image_width, image_height , image_channels);

	output_image = Image_new(image_width, image_height, image_channels);

	host_input_image_data = Image_getData(input_image);
	host_output_image_data = Image_getData(output_image);

	// allocate device buffers
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_input_image_data, sizeof(float) * image_height * image_width * image_channels));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_output_image_data, sizeof(float) * image_height * image_width * image_channels));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_tmp_image_data, sizeof(float) * image_height * image_width * image_channels));

	if(!separable) {
		CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_mask_data, mask_width * mask_width * sizeof(float)));
	}
	else {
		CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_separable_column_mask_data, mask_width * sizeof(float)));
		CUDA_CHECK_RETURN(cudaMalloc((void ** )&device_separable_row_mask_data, mask_width * sizeof(float)));
	}

	// copy memory from host to device
	CUDA_CHECK_RETURN(cudaMemcpy(device_input_image_data, host_input_image_data,
			sizeof(float) * image_height * image_width * image_channels, cudaMemcpyHostToDevice));

	if(!separable) {
		CUDA_CHECK_RETURN(cudaMemcpy(device_mask_data, host_mask_data, mask_width * mask_width * sizeof(float), cudaMemcpyHostToDevice));
		set_constant_mask(host_mask_data);
	}
	else {
		CUDA_CHECK_RETURN(cudaMemcpy(device_separable_row_mask_data, host_separable_row_mask_data, mask_width * sizeof(float), cudaMemcpyHostToDevice));
		CUDA_CHECK_RETURN(cudaMemcpy(device_separable_column_mask_data, host_separable_column_mask_data, mask_width * sizeof(float), cudaMemcpyHostToDevice));
	}


	const int BLOCK_WIDTH = 32;
	dim3 dimGrid( ceil(((float)image_width) / BLOCK_WIDTH),
			      ceil(((float)image_height) / BLOCK_WIDTH) );
	dim3 dimBlock(BLOCK_WIDTH, BLOCK_WIDTH, 1);

	printf("\nCUDA launch configuration:\n");
	printf("grid:(%d,%d,%d)\nblock:(%d,%d,%d)\n", dimGrid.x, dimGrid.y, dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z);

	printf("\n");
	printf("mask_width: %d\n", mask_width);
	printf("Mask used:\n");
	if(separable) {
		printf("separable row kernel: ");
		for(int i=0; i<mask_width; i++)
			printf("%f ", host_separable_row_mask_data[i]);
		printf("\nseparable column kernel: ");
		for(int i=0; i<mask_width; i++)
			printf("%f ", host_separable_column_mask_data[i]);
		printf("\n");

	}
	else {
		for(int i=0; i<mask_width; i++) {
			for(int j=0; j<mask_width; j++) {
				printf("%f ", host_mask_data[i*mask_width+j]);
			}
			printf("\n");
		}
	}


	/********************************************************************************************/
	// Begin timing tests...

	printf("\nApplying convolution...\n");
    // uncomment a call to use that kernel

	if(separable) {
		elapsed_time = row_separable_read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_separable_row_mask_data,
				device_tmp_image_data, image_channels, image_width, image_height, mask_width);

		//CUDA_CHECK_ERROR();
		CUDA_CHECK_RETURN(cudaDeviceSynchronize());

		elapsed_time += column_separable_read_only_cache_convolution(dimGrid, dimBlock, device_tmp_image_data, device_separable_column_mask_data,
				device_output_image_data, image_channels, image_width, image_height, mask_width);
		//CUDA_CHECK_ERROR();
	}
	else {
		elapsed_time = tiled_read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
			device_output_image_data, image_channels, image_width, image_height, mask_width);
		CUDA_CHECK_ERROR();
	}

	/*elapsed_time = channels_cycle_read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
			device_output_image_data, image_channels, image_width, image_height, mask_width);*/

	/*elapsed_time = read_only_cache_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
				device_output_image_data, image_channels, image_width, image_height, mask_width);*/

	/*elapsed_time = basic_convolution(dimGrid, dimBlock, device_input_image_data, device_mask_data,
				device_output_image_data, image_channels, image_width, image_height, mask_width);*/

	/*elapsed_time = constant_memory_convolution(dimGrid, dimBlock, device_input_image_data,
			device_output_image_data, image_channels, image_width, image_height);*/

	CUDA_CHECK_ERROR();

	// copy from device to host memory
	CUDA_CHECK_RETURN(cudaMemcpy(host_output_image_data, device_output_image_data, sizeof(float) * image_height * image_width * image_channels,
			cudaMemcpyDeviceToHost));

	printf("...elapsed time: %f ms\n", elapsed_time);

	if(write)
		PPM_export(outputfile, output_image);

	// free device memory
	cudaFree(device_input_image_data);
	cudaFree(device_output_image_data);
	if(separable) {
		cudaFree(device_tmp_image_data);
		cudaFree(device_separable_row_mask_data);
		cudaFree(device_separable_column_mask_data);
	}
	else {
		cudaFree(device_mask_data);
	}


	// free host memory
	Image_delete(output_image);
	Image_delete(input_image);

	if(separable) {
		free(host_separable_row_mask_data);
		free(host_separable_column_mask_data);
	}
	else
		free(host_mask_data);


	printf("\nexecution terminated without errors.\n");
}
