/*
 * parameters.h
 *
 *  Created on: Mar 13, 2018
 *      Author: ivan94fi
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_


extern const char *inputfile;
extern const char *outputfile;

extern float blur;
extern int edge;
extern int emboss;

extern int write;
extern int timings;
extern int separable;
extern int help;

extern int mask_width;
extern int mask_radius;

#define DEFAULT_MASK_WIDTH 5
#define DEFAULT_MASK_RADIUS  DEFAULT_MASK_WIDTH / 2
#define clamp(x) (min(max((x), 0.0), 1.0))

extern float *host_mask_data;
extern float *host_separable_row_mask_data;
extern float *host_separable_column_mask_data;

void get_cmd_opt(int argc, char* argv[]);
void generate_gaussian_kernel_2D(float **mask, float sigma);
void generate_gaussian_kernel_1D(float **row, float **col, float sigma);
void generate_edge_detect_kernel_2D(float *mask, int mask_width);
void generate_edge_detect_kernel_1D(float *row, float *col, int mask_width);

void display_help();
void display_usage();

#endif /* PARAMETERS_H_ */
