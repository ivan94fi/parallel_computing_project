/*
 * Point.h
 *
 *  Created on: Nov 10, 2017
 *      Author: ivan94fi
 */

#ifndef POINT_H_
#define POINT_H_

#include <cmath>

class Point {

public:
	Point(float, float, int cluster_id=-1);
	Point();
	virtual ~Point() = default;

	static float distance(const Point& p1, const Point& p2);

	void operator=(const Point& p);
    bool operator==(const Point &p);

	void set_x(float x);
	void set_y(float y);
	void set_xy(float x, float y);

	const float get_x() const;
    const float get_y() const;
    int get_cluster_id() const;
    void set_cluster_id(int cluster_id);

private:
    float x;
    float y;
    int cluster_id; // Id of cluster
};

#endif /* POINT_H_ */
