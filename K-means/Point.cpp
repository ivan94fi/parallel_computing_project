/*
 * Point.cpp
 *
 *  Created on: Nov 10, 2017
 *      Author: ivan94fi
 */

#include "Point.h"

Point::Point(float x, float y, int cluster_id) {
	this->x = x;
	this->y = y;
	this->cluster_id = cluster_id;
}

Point::Point() {
	x=0;
	y=0;
}

// Euclidean euclidean_distance between two points
float Point::distance(const Point& p1, const Point& p2) {
	return static_cast<float>( sqrt(pow((p1.get_x() - p2.get_x()),2) + pow((p1.get_y() - p2.get_y()),2)) );
}

void Point::operator=(const Point& p) {
	this->x = p.x;
	this->y = p.y;
	this->cluster_id = p.get_cluster_id();
}

bool Point::operator==(const Point &p) {
    return (p.get_x()==this->get_x() && p.get_y()==this->get_y());
}

const float Point::get_x() const {
    return x;
}

const float Point::get_y() const {
    return y;
}

int Point::get_cluster_id() const {
	return cluster_id;
}

void Point::set_cluster_id(int cluster_id) {
	this->cluster_id = cluster_id;
}

void Point::set_x(float x) {
	this->x = x;
}

void Point::set_y(float y) {
	this->y = y;
}

void Point::set_xy(float x, float y) {
	this->x = x;
	this->y = y;
}
