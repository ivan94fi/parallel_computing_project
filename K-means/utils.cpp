//
// Created by ivan94fi on 22/02/18.
//

#include "utils.h"
#include "parameters.h"

#include <random>
#include <algorithm>
#include <iostream>

void random_indexes(std::vector<int> &random_numbers, int points_num, int clusters_num) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<> dist(0, points_num - 1);

    for (int i = 0; i < clusters_num; i++) {
        int rnd;
        do rnd = dist(mt); while (std::find(random_numbers.begin(), random_numbers.end(), rnd) != random_numbers.end());
        random_numbers.push_back(rnd);
    }
}

void print_points(std::vector<Point> &points) {
    std::cout << "total data points: " << points.size() << std::endl;
    for (Point &point : points) {
        printf("(%f,%f) ", point.get_x(), point.get_y());
    }
    printf("\n");
}


std::vector<int> linspace(int a, int b, int N) {
    int h;
    if (N > 1)
        h = (b - a) / (N - 1);
    else
        return std::vector<int>(1, (b - a) / 2);

    std::vector<int> xs((unsigned long) N);
    std::vector<int>::iterator x;
    int val;
    for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
        *x = val;
    return xs;
}


void clear_points(Point *points) {
    for (int i = 0; i < NUM_POINTS; i++) {
        points[i].set_cluster_id(-1);
    }
}

float euclidean_distance(const Point &p1, const mean_t &p2) {
    return static_cast<float>( sqrt((p1.get_x() - p2.x) * (p1.get_x() - p2.x) +
                                    (p1.get_y() - p2.y) * (p1.get_y() - p2.y)));
}

float sq_euclidean_distance(const Point &p1, const mean_t &p2) {
    return (p1.get_x() - p2.x) * (p1.get_x() - p2.x) +
           (p1.get_y() - p2.y) * (p1.get_y() - p2.y);
}

