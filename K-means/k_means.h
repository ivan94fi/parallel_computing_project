//
// Created by ivan94fi on 22/02/18.
//

#ifndef POINTERS_PARALLEL_PC_PROJECT_K_MEANS_H
#define POINTERS_PARALLEL_PC_PROJECT_K_MEANS_H


#include <vector>
#include "Point.h"


typedef struct mean {
    float x;
    float y;
} mean_t;

// Elbow method to find the best k for k-means algorithm
int find_k(int toll, Point* points, int num_points);

// Clusters random initialization
void initialize_clusters(mean_t* means, int k, Point* points, int num_points);

// K-means algorithm
void k_means(mean_t* means, int k, Point* points, int num_points); //, int* clusters_sizes, mean_t** partial_means, int** partial_sizes);


#endif //POINTERS_PARALLEL_PC_PROJECT_K_MEANS_H
