//
// Created by ivan94fi on 22/02/18.
//

#ifndef POINTERS_PARALLEL_PC_PROJECT_UTILS_H
#define POINTERS_PARALLEL_PC_PROJECT_UTILS_H

#include "Point.h"
#include "Cluster.h"
#include "k_means.h"
#include <vector>
#include <map>

void print_clusters(std::map<int, Cluster>&);
void print_points(std::vector<Point>&);

// Reset points clusters
//void clear_points(std::vector<Point>&);

void clear_points(Point*);

// Construct random vector of indexes for initialization of cluster. All indexes of points extracted are different
void random_indexes(std::vector<int> &, int points_num, int clusters_num);

float euclidean_distance(const Point &p1, const mean_t &p2);
float sq_euclidean_distance(const Point &p1, const mean_t &p2);

std::vector<int> linspace(int a, int b, int N);


#endif //POINTERS_PARALLEL_PC_PROJECT_UTILS_H
