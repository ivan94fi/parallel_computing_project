/*
 * main.cpp
 *
 *  Created on: Nov 10, 2017
 *      Author: ivan94fi
 */


#include "Point.h"
#include "Cluster.h"
#include "k_means.h"
#include "io_utils.h"
#include "utils.h"
#include "parameters.h"

//#include "gperftools/profiler.h"

#include <iostream>
#include <sstream>
#include <random>
#include <chrono>


int main(int argc, char *argv[]) {
    //ProfilerStart("pc_proj.prof");

    get_cmd_opt(argc, argv);

#ifdef PARALLEL
    std::cout << "PARALLEL VERSION: " << NUM_THREADS << " threads" << std::endl;
#endif

    auto *test_points = (Point *) malloc(sizeof(Point) * NUM_POINTS);


    if (generatepoints) {
        std::cout << "Generating new points" << std::endl;
        std::random_device rd;
        std::mt19937 mt(rd());
        std::normal_distribution<float> dist_x(0.0, 80.0);
        std::normal_distribution<float> dist_y(0.0, 80.0);

        for (int i = 0; i < NUM_POINTS; i+=5) {
            test_points[i] = Point(dist_x(mt), dist_y(mt));
            test_points[i + 1] = Point(dist_x(mt) + 500, dist_y(mt));
            test_points[i + 2] = Point(dist_x(mt), dist_y(mt) + 500);
            test_points[i + 3] = Point(dist_x(mt) + 500, dist_y(mt) + 500);
            test_points[i + 4] = Point(dist_x(mt) + 250, dist_y(mt) + 250);
        }
    }


    std::stringstream points_filename;
    points_filename << "./" << NUM_POINTS / 1000 << "k_points.csv";

    if (writepoints) {
        std::cout << "Writing generated points on disk" << std::endl;
        write_points(test_points, points_filename.str());
    } else if (readpoints) {
        std::cout << "Reading points from disk" << std::endl;
        read_points(test_points, points_filename.str());
    }


    std::chrono::high_resolution_clock::time_point tick, tock;
    double total_time = 0;
    int iter = NUM_TEST_ITER;

    if (elbow) {
        int best_k = find_k(TOLL, test_points, NUM_POINTS);
        if (best_k < 0) {
            std::cout << "Error: find_k returned a negative value. Aborting." << std::endl;
            exit(1);
        }
        NUM_CLUSTERS = best_k;
    }

    auto *means = (mean_t *) malloc(sizeof(mean_t) * NUM_CLUSTERS);
    printf("Points created: %d\nNumber of clusters: %d\n", NUM_POINTS, NUM_CLUSTERS);


    //---------------TEST K-MEANS PERFORMANCES---------------

    for (int i = 0; i < iter; i++) {

        initialize_clusters(means, NUM_CLUSTERS, test_points, NUM_POINTS);
        tick = std::chrono::high_resolution_clock::now();
        k_means(means, NUM_CLUSTERS, test_points, NUM_POINTS);
        tock = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(
                tock - tick);
        total_time += time_span.count();
        if (i != iter - 1)
            clear_points(test_points);
        printf("run %d: time %f\n", i, time_span.count());
    }

    double average = total_time / iter;
    printf("Average in %d iterations: %f\n", iter, average);

    //-------------------------------------------------------

    // Write results on disk
    if (!nowriteresults)
        if (!write_data_to_csv(test_points, "clustering_results.csv")) {
            std::cout << "File not written. Aborting" << std::endl;
            exit(EXIT_FAILURE);
        }

    //ProfilerStop();

    // Start Python script to plot results
    int ret = system("/home/ivan94fi/anaconda2/bin/python ./plotCluster.py");
    if (ret != 0)
        printf("Python exited with error: %d", ret);

    std::cout << std::endl;
    free(means);
    return 0;
}
