//
// Created by ivan94fi on 22/02/18.
//

#include "Point.h"
#include "Cluster.h"
#include "k_means.h"
#include "io_utils.h"
#include "utils.h"
#include "parameters.h"

#include <iostream>
#include <sstream>
#include <random>
#include <chrono>
#include <getopt.h>


int generatepoints = 0;
int readpoints = 0;
int writepoints = 0;
int nowriteresults = 0;
int elbow = 0;

int NUM_CLUSTERS = 20;
int NUM_POINTS = 100000;
int NUM_TEST_ITER = 5;
int NUM_THREADS = 8;
int MAX_K = 30;
int TOLL = 1050;


void get_cmd_opt(int argc, char *argv[]) {

    std::cout << std::string(30, '*') << std::endl;
    int c;
    static struct option long_options[] =
        {
            {"generatepoints", no_argument,       &generatepoints, 1},
            {"readpoints",     no_argument,       &readpoints,     1},
            {"writepoints",    no_argument,       &writepoints,    1},
            {"elbow",          no_argument,       &elbow,          1},
            {"nowriteresults", no_argument,       &nowriteresults, 1},
            {"num_clusters",   required_argument, nullptr,         'k'},
            {"num_points",     required_argument, nullptr,         'n'},
            {"test_iter",      required_argument, nullptr,         'i'},
            {"num_threads",    required_argument, nullptr,         't'},
            {"max_k",          required_argument, nullptr,         'm'},
            {"toll",           required_argument, nullptr,         'T'},
            {nullptr,          0,                 nullptr,         0}
        };


    std::stringstream ssopt;

    while (true) {
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "grwk:n:i:t:m:T:",
                        long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c) {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != nullptr)
                    break;
                printf("option %s", long_options[option_index].name);
                if (optarg)
                    printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'g':
                printf("generate points\n");
                generatepoints = 1;
                break;

            case 'r':
                printf("read points\n");
                readpoints = 1;
                break;

            case 'w':
                printf("write points\n");
                writepoints = 1;
                break;

            case 'k':
                if(elbow == 1) {
                    printf("Error: impossible to use elbow method AND a custom number of centroids.\n");
                    //exit(1);
                }
                ssopt.str(optarg);
                ssopt >> NUM_CLUSTERS;
                ssopt.clear();
                printf("option num_clusters: %d\n", NUM_CLUSTERS);
                break;

            case 'n':
                ssopt.str(optarg);
                ssopt >> NUM_POINTS;
                ssopt.clear();
                printf("option num_points: %d\n", NUM_POINTS);
                break;

            case 'i':
                ssopt.str(optarg);
                ssopt >> NUM_TEST_ITER;
                ssopt.clear();
                printf("option test_iter: %d\n", NUM_TEST_ITER);
                break;

            case 't':
                ssopt.str(optarg);
                ssopt >> NUM_THREADS;
                ssopt.clear();
                printf("option num_threads: %d\n", NUM_THREADS);
                break;

            case 'm':
                ssopt.str(optarg);
                ssopt >> MAX_K;
                ssopt.clear();
                printf("option max_k: %d\n", MAX_K);
                break;

            case 'T':
                ssopt.str(optarg);
                ssopt >> TOLL;
                ssopt.clear();
                printf("option toll: %d\n", TOLL);
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                abort();
        }
    }

    std::cout << "flags: " << std::endl;
    std::cout << "-generatepoints:\t" << generatepoints << "\n" << \
                  "-readpoints:\t\t" << readpoints << "\n" << \
                  "-writepoints:\t\t" << writepoints << "\n" << \
                  "-nowriteresults:\t" << nowriteresults << "\n";

    std::cout << std::string(30, '*') << std::endl;
}