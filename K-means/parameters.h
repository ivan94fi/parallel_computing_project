//
// Created by ivan94fi on 22/02/18.
//

#ifndef POINTERS_PARALLEL_PC_PROJECT_PARAMETERS_H
#define POINTERS_PARALLEL_PC_PROJECT_PARAMETERS_H


#define PARALLEL


extern int generatepoints;
extern int readpoints;
extern int writepoints;
extern int nowriteresults;
extern int elbow;

extern int NUM_CLUSTERS;
extern int NUM_POINTS;
extern int NUM_TEST_ITER;
extern int NUM_THREADS;
extern int MAX_K;
extern int TOLL;


void get_cmd_opt(int argc, char* argv[]);


#endif //POINTERS_PARALLEL_PC_PROJECT_PARAMETERS_H
