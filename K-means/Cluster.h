/*
 * Cluster.h
 *
 *  Created on: Nov 10, 2017
 *      Author: ivan94fi
 */

#ifndef CLUSTER_H_
#define CLUSTER_H_

#include "Point.h"
#include <list>

class Cluster {
public:
	Cluster() = default;  // default constructor. Avoid empty constructor implementation in cpp file (c++11)
    explicit Cluster(Point& centroid);
	virtual ~Cluster() = default; //default destructor. Avoid empty destructor implementation in cpp file (c++11)

    // Calculate square error between centroid and points, for elbow method
    float calculate_square_error();
    void calculate_centroid();

    void add_point(Point*);
	void delete_point(Point*);

	std::list<Point*> get_point_list() const;

	void set_centroid(const Point &centroid);

	void set_point_list(const std::list<Point *> &point_list);

	const Point& get_centroid() const;

private:
	Point centroid;
	std::list<Point*> point_list;
};

#endif /* CLUSTER_H_ */
