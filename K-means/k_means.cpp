//
// Created by ivan94fi on 22/02/18.
//

#include <limits>
#include "k_means.h"
#include "utils.h"
#include "parameters.h"
#include <omp.h>

#include <fstream>
#include <sstream>
#include <iostream>


int find_k(int toll, Point* points, int num_points) {
    int k;
    float current_error;
    float previous_error = std::numeric_limits<float>::max();
    float *errors;
    mean_t *means;
    int num_centroids[MAX_K];
    float errs[MAX_K];

    for (k = 1; k <= MAX_K; k++) {
        printf("%d \n", k);
        means = (mean_t *) malloc(sizeof(mean_t) * k);
        errors = (float *) malloc(sizeof(float) * k);
        clear_points(points);
        initialize_clusters(means, k, points, num_points);
        k_means(means, k, points, num_points);
        for (int j = 0; j < k; j++)
            errors[j] = 0.0;

        for (int j = 0; j < num_points; j++) {
            int id = points[j].get_cluster_id();
            errors[id] += sq_euclidean_distance(points[j], means[id]);
        }

        current_error = 0.0;
        for (int j = 0; j < k; j++)
            current_error += errors[j];
        
        free(errors);
        free(means);

        //********************************************
        errs[k - 1] = current_error;
        num_centroids[k - 1] = k;

        //********************************************


        printf("%f\n", std::abs(current_error - previous_error));
        if (std::abs(current_error - previous_error) < toll) {
            return k;
        }
        previous_error = current_error;
    }
    return -1;
}

void initialize_clusters(mean_t *means, int k, Point* points, int num_points) {
    std::vector<int> random_numbers = linspace(0, num_points - 1, k);
    for (int j = 0; j < k; ++j) {
        means[j].x = 0;
        means[j].y = 0;
    }
    for (int i = 0; i < k; i++) {
        points[random_numbers[i]].set_cluster_id(i);
        means[i].x = points[random_numbers[i]].get_x();
        means[i].y = points[random_numbers[i]].get_y();
    }
}

void k_means(mean_t *means, int k, Point* points, int num_points) {
    float distance;
    float tmp;
    int owner_cluster_id = 0;
    int min_changes = 10;
    int num_changes = std::numeric_limits<int>::max();
    auto clusters_sizes = (int *) malloc(sizeof(int) * k);


#ifdef PARALLEL
    auto **partial_means = (mean_t**)malloc(sizeof(mean_t*) * NUM_THREADS);

    for(int i=0; i < NUM_THREADS; i++) {
        partial_means[i] = (mean_t*)malloc(sizeof(mean_t) * k);
    }
    auto **partial_sizes = (int**)malloc(sizeof(int*) * NUM_THREADS);
    for(int i=0; i < NUM_THREADS; i++) {
        partial_sizes[i] = (int*)malloc(sizeof(int) * k);
    }


#endif

    // Iterates until more than min_changes points change cluster
    while (num_changes > min_changes) {
        num_changes = 0;
#ifdef PARALLEL
        // Each thread is assigned a chunk of points: for each point calculate euclidean_distance from every cluster and assign new cluster
#pragma omp parallel default(none) shared(points, means, k, num_points) private(distance,tmp,owner_cluster_id) num_threads(NUM_THREADS) reduction(+:num_changes)
        {
#pragma omp for
#endif // PARALLEL
        for (unsigned i = 0; i < num_points; i++) {
            distance = std::numeric_limits<float>::max();

            // Calculate the id of nearest cluster. owner_cluster_id is the nearest cluster so far
            for (int j = 0; j < k; j++) {
                tmp = euclidean_distance(points[i], means[j]);
                if (tmp < distance) {
                    distance = tmp;
                    owner_cluster_id = j;
                }
            }

            // Current point's cluster id
            int old_id = points[i].get_cluster_id();
            // New point's cluster id
            int new_id = owner_cluster_id;

            // First iteration, all points are initialized with cluster id -1: change to the new one
            // Or: the cluster id has changed
            if (old_id == -1 || new_id != old_id) {
                points[i].set_cluster_id(new_id);
                num_changes++; // A point has changed cluster
            }
        } // END pragma omp for
#ifdef PARALLEL
        } // END pragma omp parallel
#endif

        // Re-initialize clusters
        for (int i = 0; i < k; i++) {
            clusters_sizes[i] = 0;
            means[i].x = 0;
            means[i].y = 0;
        }

#ifdef PARALLEL
        for(int i=0; i < NUM_THREADS; i++) {
            for (int j = 0; j < k; j++) {
                partial_means[i][j].x = 0.0;
                partial_means[i][j].y = 0.0;
            }
        }
        for(int i=0; i < NUM_THREADS; i++) {
            for (int j = 0; j < k; j++) {
                partial_sizes[i][j] = 0;
            }
        }
        

#pragma omp parallel num_threads(NUM_THREADS)
        {
#pragma omp for
            for (int i = 0; i < num_points; i++) {
                int tid = omp_get_thread_num();
                Point tmp_point = points[i];
                partial_means[tid][tmp_point.get_cluster_id()].x += tmp_point.get_x();
                partial_means[tid][tmp_point.get_cluster_id()].y += tmp_point.get_y();
            }

            // Count how many points are in each cluster
#pragma omp for
            for (int i = 0; i < num_points; i++) {
                int tid = omp_get_thread_num();
                partial_sizes[tid][points[i].get_cluster_id()]++;
            }

        }
        for(int i=0; i < NUM_THREADS; i++) {
            for (int j = 0; j < k; j++) {
                means[j].x += partial_means[i][j].x;
                means[j].y += partial_means[i][j].y;
            }
        }
        for(int i=0; i < NUM_THREADS; i++) {
            for (int j = 0; j < k; j++) {
                clusters_sizes[j] += partial_sizes[i][j];
            }
        }

        for (int i = 0; i < k; i++) {
            means[i].x = means[i].x / clusters_sizes[i];
            means[i].y = means[i].y / clusters_sizes[i];
        }

    } // end while num_changes > min_changes
    

    for(int i=0; i < NUM_THREADS; i++) {
        free(partial_means[i]);
        free(partial_sizes[i]);
    }
    free(partial_means);
    free(partial_sizes);
    free(clusters_sizes);
#else

        // Count how many points are in each cluster
        for (int i = 0; i < num_points; i++) {
            clusters_sizes[points[i].get_cluster_id()]++;
        }

        // Calculate new centroids coordinates
        for (int i = 0; i < num_points; ++i) {
            Point tmp_point = points[i];
            means[tmp_point.get_cluster_id()].x += tmp_point.get_x();
            means[tmp_point.get_cluster_id()].y += tmp_point.get_y();
        }

        for (int i = 0; i < k; i++) {
            means[i].x = means[i].x / clusters_sizes[i];
            means[i].y = means[i].y / clusters_sizes[i];
        }

    } // end while num_changes > min_changes
    free(clusters_sizes);

#endif
}