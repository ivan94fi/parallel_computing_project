//
// Created by ivan94fi on 22/02/18.
//

#include "io_utils.h"
#include "Cluster.h"
#include "parameters.h"


#include <fstream>
#include <sstream>
#include <map>
#include <iostream>


bool read_points(Point* points, const std::string &filename) {
    std::string line;
    std::ifstream infile(filename);
    if (!infile.is_open()) {
        perror("error while opening file");
        return false;
    }
    int i=0;
    while(std::getline(infile, line)) {
        auto pos = line.find(' ');
        std::string x = line.substr(0,pos);
        std::string y = line.substr(pos+1);
        points[i] = Point(std::stof(x), std::stof(y));
        i++;
    }
    return true;
}


bool write_points(Point* points, const std::string &filename) {
    std::ofstream outfile(filename);
    if (!outfile.is_open()) {
        perror("error while opening file");
        return false;
    }
    std::stringstream line;
    for(int i=0; i < NUM_POINTS; i++) {
        line << points[i].get_x() << " " << points[i].get_y() << '\n';
    }
    outfile << line.str();
    if (outfile.bad()) {
        perror("error while reading file");
        return false;
    }
    outfile.close();
    return true;
}


bool write_data_to_csv(Point* points, const std::string &filename) {
    std::ofstream outfile(filename);
    if (!outfile.is_open()) {
        perror("error while opening file");
        return false;
    }

    std::map<int,Cluster> clusters;
    for(int i=0; i<NUM_POINTS; i++) {
        int id = points[i].get_cluster_id();
        if( id == -1 ) {
            std::cerr << "---------- Found id=-1\n";
            exit(200);
        }
        clusters[points[i].get_cluster_id()].add_point(&points[i]);
    }

    for (auto &cluster : clusters)
        cluster.second.calculate_centroid();

    for (auto& cluster : clusters) {
        std::list<Point*> list = cluster.second.get_point_list();
        std:: stringstream line;
        line << cluster.first << ' ' << cluster.second.get_centroid().get_x() << ',' << cluster.second.get_centroid().get_y() << ' ';
        for (auto& point : list) {
            line << point->get_x() << ',' << point->get_y() << ' ';
        }
        line << '\n';
        outfile << line.str();
    }
    if (outfile.bad()) {
        perror("error while reading file");
        return false;
    }
    outfile.close();
    return true;
}