/*
 * Cluster.cpp
 *
 *  Created on: Nov 10, 2017
 *      Author: ivan94fi
 */

#include "Cluster.h"
#include <iostream>
#include <algorithm>

Cluster::Cluster(Point& centroid) {
	this->centroid = centroid;
    point_list.push_back(&centroid);
}

float Cluster::calculate_square_error() {
    float error = 0;
    for (auto& point : point_list)
        error += pow(Point::distance(centroid, *point), 2);
    return error;
}

void Cluster::add_point(Point* p) {
        point_list.push_back(p);
}

void Cluster::delete_point(Point* p){
    for( auto it=point_list.begin(); it!=point_list.end(); it++ ) {
        if(p->get_x() == (*it)->get_x() && p->get_y() == (*it)->get_y()) {
            point_list.erase(it);
            return;
        }
    }
}

void Cluster::calculate_centroid() {
    float x_sum = 0;
    float y_sum = 0;
    auto size = static_cast<int>(point_list.size());
    if(size>0) {
        for (auto& it : point_list) {
            x_sum += (*it).get_x();
            y_sum += (*it).get_y();
        }

        int new_id = centroid.get_cluster_id();
        centroid = Point(x_sum/size, y_sum/size, new_id);
    }
}

std::list<Point*> Cluster::get_point_list() const {
    return point_list;
}

const Point &Cluster::get_centroid() const {
    return centroid;
}

void Cluster::set_centroid(const Point &centroid) {
    Cluster::centroid = centroid;
}

void Cluster::set_point_list(const std::list<Point *> &point_list) {
    Cluster::point_list = point_list;
}
