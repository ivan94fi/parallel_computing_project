//
// Created by ivan94fi on 22/02/18.
//

#ifndef POINTERS_PARALLEL_PC_PROJECT_IO_UTILS_H
#define POINTERS_PARALLEL_PC_PROJECT_IO_UTILS_H

#include "Point.h"

#include <vector>
#include <string>


bool load_data_from_csv(Point* points, const std::string &);
bool write_data_to_csv(Point* points, const std::string &filename);

bool write_points(Point* points, const std::string &filename);
bool read_points(Point* points, const std::string &filename);



#endif //POINTERS_PARALLEL_PC_PROJECT_IO_UTILS_H
