import csv
import pandas as pd
import seaborn
from matplotlib import pyplot as plt

dataset = pd.DataFrame(columns=["cluster", "X_centroid", "Y_centroid", "X", "Y", "color"])
seaborn.set(style="ticks")

with open("../clustering_results.csv", "rb") as csvfile:
    perfumereader = csv.reader(csvfile, delimiter=' ')
    X = []
    Y = []
    X_centroid = []
    Y_centroid = []
    cluster = []
    count = 0
    for row in perfumereader:
        count += 1
        for item in row:
            # print(row[0])
            if item == row[0] or item == row[1] or item == "":
                continue
            else:
                cluster.append(row[0])
                X_centroid.append(row[1].split(",")[0])
                Y_centroid.append(row[1].split(",")[1])
                X.append(item.split(",")[0])
                Y.append(item.split(",")[1])

    # print X_centroid
    # print Y_centroid
    # with open('cluster_file', 'wb') as clusterfile:
    #     clusterfile.writelines(X_centroid)
    #     clusterfile.writelines(Y_centroid)
    dataset["cluster"] = cluster
    dataset["X_centroid"] = X_centroid
    dataset["Y_centroid"] = Y_centroid
    dataset["X"] = X
    dataset["Y"] = Y

    dataset[["X", "Y"]] = dataset[["X", "Y"]].apply(pd.to_numeric)

    fg = seaborn.FacetGrid(data=dataset, hue="cluster", aspect=1.61)
    fg.map(plt.scatter, "X", "Y", s=1)
    fg.map(plt.scatter, "X_centroid", "Y_centroid", marker="x", color="black", s=40, linewidth="1")

    # plt.savefig('plot_' + str(count) + '.png')
    plt.xlabel('')
    plt.ylabel('')
    plt.show()

# dataset[["X","Y"]].to_csv("newDataset.csv",sep=" ",index=False)
