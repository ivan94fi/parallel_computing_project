# Parallel Computing Project

Parallel Computing Project of Simone Magistri and Ivan Prosperi. 
The midterm project is the parallel implementation of the K-means clustering algorithm. 
The final project is the parallel implementation of the convolution operation for image processing.


## Running the programs


### K-means

To compile the program: 

```
cd K-means
```

```
g++ -O3 -std=c++11 main.cpp Cluster.cpp io_utils.cpp k_means.cpp parameters.cpp Point.cpp utils.cpp -o k-means -fopenm
```

To run the program:

```
./k-means [options] 
```

The available options are:

```
    -g, --generatepoints        generate a new dataset and write it to disk as a csv.
    -r, --readpoints            read the points from disk.
    -w, --writepoints           write a csv with the current points.
    --elbow                     use elbow mwthod to find optimal k.
    --nowriteresults            debug option: does not write clustering results.
    -k, --num_clusters          specify an apriori number of clusters.
    -n, --num_points            specify the number of points to use.
    -i, --test_iter             number of iterations for timing accuracy.
    -t, --num_threads           specify a number of threads.
    -m, --max_k                 maximum iterations count for k-means (stopping criterion).
    -T, --toll                  tolerance value for elbow method.
```

**Warning**: to use the sequential approach it is necessary to comment line 9 of parameters.h file: ``` #define PARALLEL ```. That way the ``` omp parallel ``` sections will be replaced by normal for.
### Image processing

#### CUDA

To compile the program:

```
cd CUDA_Convolution
```

```
nvcc -O3 -std=c++11  convolution.cu main.cpp Image.cpp parameters.cpp PPM.cpp test_convolution.cu test_timings.cu -o CUDA_Convolution
```

To run the program:

```
./CUDA_Convolution [options] [inputfilename] [outputfilename]
```

where options is one or more of:

```
    -b VALUE, --blur VALUE           use uniform blur: possible values are 1, 2 and 3.
    -g STDDEV, --gauss STDDEV        use gaussian blur with standard deviation STDDEV (may be a real number).
    -e VALUE, --edge VALUE           use an edge detect (Sobel-like) kernel: possible values are 1, 2 and 3.

    --emboss                         use an embossing kernel (--separable has no effect).
    --timings                        test all cuda kernels and print timings.
    --separable                      use a separable kernel approach.
    -h, --help                       print an help message.
```

The program is able to read PPM image files. There are some samples in CUDA_Convolution/images folders.

#### Java

To compile the program:
```
cd Convolution_Java/src
```

```
javac Main.java

```
To run the program:
```
java Main [option] [inputfilepath]

```

where option is one of:

```
    -seq               apply sequential 2D convolution. 
    -par               apply parallel 2D convolution.
    -seq_sep           apply sequential separable convolution.
    -par_sep           apply parallel separable convolution.
    -h                 display an help message.
```

The program is tested for jpg image files. The are some samples in Java_Convolution/InputImage.

## Authors

* **Simone Magistri**
* **Ivan Prosperi**

